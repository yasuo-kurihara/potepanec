Spree::Product.class_eval do
  scope :eager_load_products, -> { includes(master: [:default_price, :images]) }
  scope :related_products, -> product { includes(:taxons).where(spree_taxons: { id: product.taxons.ids }) }
  scope :exclude_product, -> product_id { where.not(id: product_id) }

  # 対象商品一覧取得
  def self.get_taxon_products(taxon)
    taxon_ids = taxon.leaf? ? taxon.id : taxon.leaves.ids
    eager_load_products.includes(:taxons).where(spree_products_taxons: { taxon_id: taxon_ids })
  end

  # 関連商品を取得
  def related_products(product, count)
    Spree::Product.eager_load_products.related_products(product).exclude_product(product.id).limit(count)
  end

  # 新着商品取得
  def self.new_arrivals(count)
    eager_load_products.available.order(available_on: :desc).limit(count)
  end
end
