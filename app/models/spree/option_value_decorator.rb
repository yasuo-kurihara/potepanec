Spree::OptionValue.class_eval do
  def eager_load_variants
    variants.includes(:default_price, :images)
  end
end
