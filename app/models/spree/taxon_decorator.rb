Spree::Taxon.class_eval do
  # 各カテゴリーの商品数取得
  def products_quantity
    return products.count if leaf?
    # category・Brandなど項目の商品数取得
    leaves.includes(:products).map { |l| l.products.count }.inject(:+)
  end
end
