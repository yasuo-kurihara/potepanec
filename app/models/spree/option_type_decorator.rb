Spree::OptionType.class_eval do
  scope :search_option_value, -> option_type { find_by(name: option_type).option_values }
end
