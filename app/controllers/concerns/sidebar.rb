module Sidebar
  def sidebar_search
    @taxonomies = Spree::Taxonomy.includes(:taxons)
    @search_colors = Spree::OptionType.search_option_value('tshirt-color')
  end
end
