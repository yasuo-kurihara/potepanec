class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_COUNT = 4
  include Sidebar

  def index
    @option_value_color = Spree::OptionValue.find_by(name: params[:color])
    sidebar_search
    products
  end

  def show
    @product = Spree::Product.find(params[:id])
    # 関連商品
    @related = @product.related_products(@product, RELATED_PRODUCTS_COUNT)
    @variant_number = params[:variant]
    variants
  end

  private

  def products
    if params[:color]
      return flash.now[:error] = "検索エラー.  別の方法で検索してください" if @option_value_color.nil?
      @products = @option_value_color.eager_load_variants
    else
      @products = Spree::Product.eager_load_products
    end
  end

  def variants
    unless @variant_number.nil?
      @variants = Spree::Variant.includes(:images).find(@variant_number)
    end
  end
end
