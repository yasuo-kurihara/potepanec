class Potepan::HomeController < ApplicationController
  NEW_ARRIVALS_PRODUCTS_COUNT = 8
  def index
    # 新着商品取得
    @new_arrivals_products = Spree::Product.new_arrivals(NEW_ARRIVALS_PRODUCTS_COUNT)
  end
end
