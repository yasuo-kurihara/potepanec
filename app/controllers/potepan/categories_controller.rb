class Potepan::CategoriesController < ApplicationController
  include Sidebar

  def show
    # 選択されたカテゴリーの取得
    @taxon = Spree::Taxon.find_by_id(params[:id])
    return routing_error unless @taxon
    sidebar_search
    # 対象商品一覧取得
    @products = Spree::Product.get_taxon_products(@taxon)
  end
end
