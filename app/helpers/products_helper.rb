module ProductsHelper
  def product_link(product)
    if product.respond_to?(:product_id)
      potepan_product_path(product.product_id, variant: product.id)
    else
      potepan_product_path(product.id)
    end
  end

  def product_main_image_active?(index, variant_number)
    return false unless variant_number.nil?
    index.zero?
  end

  def product_thumb_image_number(index, image_size)
    index + image_size
  end
end
