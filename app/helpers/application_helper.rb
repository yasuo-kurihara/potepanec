module ApplicationHelper
  # Returns the complete title for each page.
  def full_title(page_title:)
    base_title = 'Potepanec'
    if page_title.empty?
      base_title
    else
      page_title
    end
  end
end