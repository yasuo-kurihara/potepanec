require 'spree/testing_support/factories/product_factory'

FactoryGirl.define do
  # 日付違いの商品
  factory :new_product, parent: :product do
    sequence(:available_on) { |n| n.days.ago }
  end
end
