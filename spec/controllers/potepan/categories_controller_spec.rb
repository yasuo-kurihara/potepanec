require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:taxon) { create :taxon }
    let(:taxonomies) { Spree::Taxonomy.includes(:taxons) }
    let!(:color) { create(:option_type, name: 'tshirt-color') }

    # 商品10個作成
    let(:products) do
      create_list(:product, 10) do |product|
        product.taxons << taxon
      end
    end

    before { get :show, params: { id: taxon.id } }

    it 'アクセスが成功しているのか' do
      expect(response).to have_http_status(:success)
    end

    it '正常にviewファイルが呼び出せているか' do
      expect(response).to render_template :show
    end

    it '@taxonのインスタンス変数が適切かどうか' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '@taxonomiesのインスタンス変数が適切かどうか' do
      expect(assigns(:taxonomies)).to eq taxonomies
    end

    it '@productsのインスタンス変数が適切かどうか' do
      expect(assigns(:products)).to eq products
    end
  end
end
