require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "GET #index" do
    before do
      get :index
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'ステータスコード200が返ること' do
      expect(response.status).to eq(200)
    end

    it '正常にviewファイルを呼び出せているか' do
      expect(response).to render_template :index
    end
  end

  describe "新着商品を表示する(@new_arrivals_products)" do
    # 新着商品8個作成
    let(:eight_products) { create_list(:product, 8) }
    # 新着商品9個作成
    let(:nine_products) { create_list(:product, 9) }
    # 新着商品7個作成
    let(:seven_products) { create_list(:product, 7) }
    # 日付違いの商品
    let(:desc_products) { create_list(:new_product, 8) }

    describe '取得される新着商品数(Max表示８個)' do
      subject { assigns(:new_arrivals_products).count }

      let(:limit_count_of_new_arrivals_products) { Potepan::HomeController::NEW_ARRIVALS_PRODUCTS_COUNT }

      shared_examples 'new_arrivals_products_count' do
        it '商品数チェック' do
          get :index
          is_expected.to be <= limit_count_of_new_arrivals_products
        end
      end

      context '新着商品が８個の場合８個表示する' do
        it_behaves_like 'new_arrivals_products_count' do
          let(:new_arrivals_products) { eight_products }
        end
      end

      context '新着商品が７個の場合７個表示する' do
        it_behaves_like 'new_arrivals_products_count' do
          let(:new_arrivals_products) { seven_products }
        end
      end

      context '新着商品が９個の場合８個表示する' do
        it_behaves_like 'new_arrivals_products_count' do
          let(:new_arrivals_products) { nine_products }
        end
      end
    end

    describe '商品データのavailable_onの状態' do
      # available_onがnilの商品
      let!(:nil_product) { create(:product, available_on: '') }
      # available_onが未来の日付の商品
      let!(:future_product1) { create(:product, available_on: 1.day.from_now) }
      let!(:future_product2) { create(:product, available_on: 1.month.from_now) }

      context 'available_onの値が空(null)の商品は' do
        it '表示しない' do
          get :index
          expect(assigns(:new_arrivals_products)).not_to include(nil_product)
        end
      end

      context 'available_onの値が未来の日付の商品は' do
        it '表示しない' do
          get :index
          expect(assigns(:new_arrivals_products)).not_to include(future_product1)
          expect(assigns(:new_arrivals_products)).not_to include(future_product2)
        end
      end
    end

    context '新着商品の表示順' do
      it '新着商品の表示順は日付が新しい順に8件表示される' do
        get :index
        expect(assigns(:new_arrivals_products)).to match(desc_products)
      end
    end
  end
end
