require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'Get #index' do
    let!(:color) { create(:option_type, name: 'tshirt-color') }
    let(:search_colors) { [white, black] }
    let(:white) { create(:option_value, name: 'White', option_type_id: color.id) }
    let(:black) { create(:option_value, name: 'Black', option_type_id: color.id) }
    let!(:products) { create_list(:product, 4) }

    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'ステータスコード200が返ること' do
      get :index
      expect(response.status).to eq(200)
    end

    it '正常にviewファイルを呼び出せているか' do
      get :index
      expect(response).to render_template :index
    end

    it "@search_colorsのインスタンス変数が適切かどうか" do
      get :index
      expect(assigns(:search_colors)).to eq search_colors
    end

    it "@productsのインスタンス変数が適切かどうか" do
      get :index
      expect(assigns(:products)).to eq products
    end

    describe 'URLのクエリーによる色の絞り込み' do
      let!(:color_lists) { [red, blue] }
      let(:red_variants) { create_list(:variant, 2) }
      let(:blue_variants) { create_list(:variant, 2) }

      let(:red) do
        create(:option_value, name: 'Red', option_type_id: color.id) do |red|
          red.variants << red_variants
        end
      end

      let(:blue) do
        create(:option_value, name: 'Blue', option_type_id: color.id) do |blue|
          blue.variants << blue_variants
        end
      end

      context 'クエリーの値について' do
        it 'クエリーの値が正しい場合, ステータスコード200が返ること' do
          get :index, params: { color: 'Red' }
          expect(response.status).to eq 200
        end

        it 'クエリーの値が正しくない場合, flash.now[:error]のメッセージが返される' do
          get :index, params: { color: 'iro' }
          expect(flash.now[:error]).to eq("検索エラー.  別の方法で検索してください")
        end
      end

      context "params[:color]がRedのとき" do
        it "Redのバリエーションがあるproductsを取得" do
          get :index, params: { color: 'Red' }
          expect(assigns(:products)).to match_array red.variants
        end

        it "Blueのバリエーションがあるproductsは含まれない" do
          get :index, params: { color: 'Red' }
          expect(assigns(:products)).not_to match_array blue.variants
        end
      end
    end
  end

  describe 'Get #show' do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it 'ステータスコード200が返ること' do
      expect(response.status).to eq(200)
    end

    it '正常にviewファイルを呼び出せているか' do
      expect(response).to render_template :show
    end

    it "@productのインスタンス変数が適切かどうか" do
      expect(assigns(:product)).to eq product
    end

    describe 'URLのクエリー(variant)による表示' do
      let(:product) { create(:product) }
      let(:variant) { create(:variant, product: product) }

      before do
        get :show, params: { id: variant.product_id, variant: variant.id }
      end

      it 'ステータスコード200が返ること' do
        expect(response.status).to eq(200)
      end

      it '正常にviewファイルを呼び出せているか' do
        expect(response).to render_template :show
      end

      it "@productのインスタンス変数が適切かどうか" do
        expect(assigns(:product)).to eq variant.product
      end
    end

    describe '@related(related products)' do
      let(:taxonomy_category) { create(:taxonomy, name: 'Categories') }
      let(:taxonomy_brands)   { create(:taxonomy, name: 'Brands') }

      let(:clothing_taxon)    { create(:taxon, name: 'Clothing', taxonomy: taxonomy_category) }
      let(:bags_taxon)        { create(:taxon, name: 'Bags', taxonomy: taxonomy_category) }
      let(:mugs_taxon)        { create(:taxon, name: 'Mugs', taxonomy: taxonomy_category) }
      let(:apatch_taxon)      { create(:taxon, name: 'Apache', taxonomy: taxonomy_brands) }
      let(:rails_taxon)       { create(:taxon, name: 'Rails', taxonomy: taxonomy_brands) }
      let(:ruby_taxon)        { create(:taxon, name: 'Ruby', taxonomy: taxonomy_brands) }

      # 関連商品4個作成
      let(:bugs_products) do
        create_list(:product, 4) do |product|
          product.taxons << bags_taxon
          product.taxons << apatch_taxon
        end
      end

      # 関連商品5個作成
      let(:mugs_products) do
        create_list(:product, 5) do |product|
          product.taxons << mugs_taxon
          product.taxons << rails_taxon
        end
      end

      # 関連商品3個作成
      let(:shirt_products) do
        create_list(:product, 3) do |product|
          product.taxons << clothing_taxon
          product.taxons << ruby_taxon
        end
      end

      it "関連する製品には同じカテゴリーの製品が含まれる" do
        bugs_product = bugs_products.first
        related_products = (bugs_products - [bugs_product])
        get :show, params: { id: bugs_product.id }
        expect(assigns(:related)).to match_array related_products
      end

      it '他のカテゴリーは含まない' do
        mugs_product = mugs_products.first
        get :show, params: { id: mugs_product.id }
        expect(assigns(:related)).not_to include(bugs_products)
      end

      describe '取得される関連商品の数' do
        subject { assigns(:related).count }

        let(:limit_count_of_related_products) { Potepan::ProductsController::RELATED_PRODUCTS_COUNT }

        shared_examples "related_products_count" do
          it "商品数チェック" do
            get :show, params: { id: related_products.first.id }
            is_expected.to be <= limit_count_of_related_products
          end
        end

        context '関連商品が4個の場合は4個表示される' do
          it_behaves_like "related_products_count" do
            let(:related_products) { bugs_products }
          end
        end

        context '関連商品が5個の場合は4個表示される' do
          it_behaves_like "related_products_count" do
            let(:related_products) { mugs_products }
          end
        end

        context '関連商品が3個の場合は3個表示される' do
          it_behaves_like "related_products_count" do
            let(:related_products) { shirt_products }
          end
        end
      end
    end
  end
end
