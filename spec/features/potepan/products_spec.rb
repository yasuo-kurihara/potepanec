require 'rails_helper'

RSpec.feature "Visiting Products", :type => :feature, inaccessible: true do
  let!(:products) { Spree::Product.eager_load_products }
  let!(:color) { create(:option_type, name: 'tshirt-color') }

  it '商品一覧ページのtitle表示がALLになる' do
    visit potepan_products_url
    expect(page).to have_selector 'h2', text: 'ALL'
  end

  context "商品一覧ページのパラメーターcolorがRedの時" do
    let(:red_variants) { create_list(:variant, 2) }
    let(:red_products) { create(:option_value, name: 'Red', option_type_id: color.id) }
    let!(:products) { red_products }

    it 'タイトルの表示がRedになる' do
      visit potepan_products_url('color': 'Red')
      expect(page).to have_selector 'h2', text: 'Red'
    end
  end

  context "詳細ページ" do
    let!(:product) { create(:product, id: 3, name: "product 1") }
    let!(:custom_image) { create(:image, attachment_file_name: "product1.jpg") }
    let!(:product_images) { product.images << custom_image }
    let(:variant) { create(:variant, id: 10) }
    let!(:images) { create_list(:image, 4) }
    let!(:variant_images) { variant.images << images }

    it 'product nameの確認' do
      visit potepan_product_url(3)
      expect(page).to have_selector 'h2', text: "product 1"
    end

    it "クエリがない場合、product image取得" do
      visit potepan_product_url(3)
      expect(page).to have_xpath("//img[contains(@src,'product1')]")
    end

    it "クエリーvariantの指定があった時は、imageがvariantを取得" do
      visit potepan_product_url(id:3, variant: 10)
      expect(page).to have_xpath("//img[contains(@src,'thinking-cat')]")
    end
  end
end